## Install Instructions

- Clone the repository: `git clone https://gitlab.com/jolbe/tweet_saver.git`
- Move to the cloned repository: `cd tweet_saver`
- Install node packages: `npm install`
- Start development web server: `npm start`
