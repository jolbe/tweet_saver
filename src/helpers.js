export const TWITTER_API_URL = "https://tweetsaver.herokuapp.com";
// ?q=obama&callback=yourJSONPCallbackFn&count=10

export function getTwitterApiUrl(query = "", count = 10) {
  return `${TWITTER_API_URL}/?q=${query}&count=${count}`;
}
