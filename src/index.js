import React from "react";
import { render } from "react-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import "./index.css";
import Router from "./components/Router";

render(<Router />, document.getElementById("root"));
