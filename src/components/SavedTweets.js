import React from "react";
import styled from "styled-components";
import { Droppable } from "react-beautiful-dnd";
import TweetCard from "./TweetCard";

const Container = styled.div`
  display: flex;
  flex-direction: column;
  margin: 8px;
  border: 1px solid lightgray;
  border-radius: 2px;
  width: 50%;
`;
const Title = styled.h5`
  padding: 8px;
`;
const TweetList = styled.div`
  padding: 8px;
  flex-grow: 1;
`;

class SavedTweets extends React.Component {
  render() {
    return (
      <Container>
        <Title>Saved Tweets</Title>
        <Droppable droppableId="droppable-saved">
          {provided => (
            <TweetList
              innerRef={provided.innerRef}
              {...provided.droppableProps}
            >
              {this.props.tweets.map((tweet, index) => (
                <TweetCard key={tweet.id} tweet={tweet} index={index} />
              ))}
              {provided.placeholder}
            </TweetList>
          )}
        </Droppable>
      </Container>
    );
  }
}

export default SavedTweets;
