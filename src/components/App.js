import React, { Component } from "react";
import { DragDropContext } from "react-beautiful-dnd";
import fetchJsonp from "fetch-jsonp";
import styled from "styled-components";

import Header from "./Header";
import SearchBar from "./SearchBar";
import SearchResults from "./SearchResults";
import SavedTweets from "./SavedTweets";

import { TWITTER_API_URL, getTwitterApiUrl } from "../helpers";

const Container = styled.div`
  display: flex;
`;

class App extends Component {
  state = {
    searchTerm: "",
    tweets: [],
    savedTweets: []
  };

  onDragEnd = result => {
    // TODO: reorder our column
    console.info(result);
    const { destination, source, draggableId } = result;

    if (!destination) {
      return;
    }

    if (
      destination.droppableId == source.droppableId &&
      destination.index == source.index
    ) {
      return;
    }

    if (
      destination.droppableId == source.droppableId &&
      destination.droppableId == "droppable-searched"
    ) {
      return;
    }

    if (
      destination.droppableId == "droppable-saved" &&
      (source.droppableId == "droppable-searched" ||
        source.droppableId == "droppable-saved")
    ) {
      let { tweets, savedTweets } = this.state;
      const removeFrom =
        source.droppableId == "droppable-searched" ? tweets : savedTweets;
      let removed = removeFrom.splice(source.index, 1)[0];
      savedTweets.splice(destination.index, 0, removed);
      this.setState({ tweets, savedTweets });
      localStorage.setItem("savedTweets", JSON.stringify(savedTweets));
    }
  };

  searchTweets = searchTerm => {
    this.setState({ searchTerm });
    fetchJsonp(getTwitterApiUrl(searchTerm))
      .then(response => {
        return response.json();
      })
      .then(json => {
        console.log(json);
        this.setState({ tweets: json.tweets });
      })
      .catch(err => {
        console.error(err);
      });
  };

  saveTweet = tweet => {
    let savedTweets = this.state.savedTweets;
    savedTweets.push(tweet);
    this.setState({ savedTweets });
    localStorage.setItem("savedTweets", JSON.stringify(savedTweets));
  };

  componentDidMount() {
    // load saved tweets
    let savedTweets = JSON.parse(localStorage.getItem("savedTweets")) || [];
    this.setState({ savedTweets });
  }

  render() {
    return (
      <div className="container-fluid">
        <Header />
        <SearchBar searchTweets={this.searchTweets} />
        <DragDropContext onDragEnd={this.onDragEnd}>
          <Container>
            <SearchResults
              searchTerm={this.state.searchTerm}
              tweets={this.state.tweets}
              saveTweet={this.saveTweet}
            />
            <SavedTweets tweets={this.state.savedTweets} />
          </Container>
        </DragDropContext>
      </div>
    );
  }
}

export default App;
