import React from "react";

class SearchBar extends React.Component {
  searchInput = React.createRef();

  searchTweets = event => {
    event.preventDefault();
    let searchTerm = this.searchInput.current.value;
    this.props.searchTweets(searchTerm);
    event.currentTarget.reset();
  };

  render() {
    return (
      <form className="search-tweets" onSubmit={this.searchTweets}>
        <div className="form-row align-items-center">
          <div className="col-sm-4 my-1">
            <input
              name="searchInput"
              className="form-control"
              type="text"
              ref={this.searchInput}
              required
              placeholder="Search for tweets..."
            />
          </div>
          <div className="col-auto my-1">
            <button type="submit" className="btn btn-primary">
              🔍 Search
            </button>
          </div>
        </div>
      </form>
    );
  }
}

export default SearchBar;
