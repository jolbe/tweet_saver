import React from "react";

const Header = props => (
  <nav className="navbar navbar-dark bg-dark mb-4">
    <a className="navbar-brand" href="#">
      <img
        src="http://www.clixmarketing.com/blog/wp-content/uploads/2015/08/twitter-logo-blue.png"
        width="30"
        height="30"
        className="d-inline-block align-top mr-3"
        alt=""
      />
      Tweet Saver
    </a>
  </nav>
);

export default Header;
