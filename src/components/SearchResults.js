import React from "react";
import styled from "styled-components";
import { Droppable } from "react-beautiful-dnd";
import TweetCard from "./TweetCard";

const Container = styled.div`
  margin: 8px;
  border: 1px solid lightgray;
  border-radius: 2px;
  width: 50%;
`;
const Title = styled.h5`
  padding: 8px;
`;
const TweetList = styled.div`
  padding: 8px;
`;

class SearchResults extends React.Component {
  render() {
    return (
      <Container>
        <Title>Search results for: {this.props.searchTerm}</Title>
        <Droppable droppableId="droppable-searched">
          {provided => (
            <TweetList
              innerRef={provided.innerRef}
              {...provided.droppableProps}
            >
              {this.props.tweets.map((tweet, index) => (
                <TweetCard
                  key={tweet.id}
                  tweet={tweet}
                  index={index}
                  saveTweet={this.props.saveTweet}
                />
              ))}
              {provided.placeholder}
            </TweetList>
          )}
        </Droppable>

        {this.props.searchTerm &&
          this.props.tweets.length == 0 && <div>No results found</div>}
      </Container>
    );
  }
}

export default SearchResults;
