import React from "react";
import { Draggable } from "react-beautiful-dnd";
import styled from "styled-components";

const Container = styled.div``;
const UserName = styled.span`
  font-size: 14px;
  color: #14171a;
  margin-right: 5px;

  &:hover {
    color: #abb8c2;
    text-decoration: underline;
  }
`;
const ScreenName = styled.span`
  font-size: 14px;
  color: #657786;
`;

const CreatedAt = styled.span`
  font-size: 14px;
  color: #657786;
`;

class TweetCard extends React.Component {
  formatDate = date => {
    const months = [
      "Jan",
      "Feb",
      "Mar",
      "Apr",
      "May",
      "Jun",
      "Jul",
      "Aug",
      "Sep",
      "Oct",
      "Nov",
      "Dec"
    ];
    let created = new Date(date);
    return `${created.getDate()} ${
      months[created.getMonth()]
    } ${created.getFullYear()}`;
  };

  render() {
    const { tweet } = this.props;
    return (
      <Draggable draggableId={tweet.id} index={this.props.index}>
        {provided => (
          <Container
            className="card bg-light mb-3"
            {...provided.draggableProps}
            {...provided.dragHandleProps}
            innerRef={provided.innerRef}
          >
            <div className="card-body">
              <img
                className="rounded float-left mr-3"
                src={tweet.user.biggerProfileImageURL}
                alt="User image"
              />
              <h5 className="card-title">
                <a
                  href={`https://twitter.com/${tweet.user.screenName}`}
                  style={{ textDecoration: "none" }}
                >
                  <UserName>
                    <strong>{tweet.user.name}</strong>
                  </UserName>
                  <ScreenName>@{tweet.user.screenName}</ScreenName>
                </a>
                <div className="float-right">
                  <CreatedAt>{this.formatDate(tweet.createdAt)}</CreatedAt>
                </div>
              </h5>
              <p className="card-text">{tweet.text}</p>
            </div>
          </Container>
        )}
      </Draggable>
    );
  }
}

export default TweetCard;
