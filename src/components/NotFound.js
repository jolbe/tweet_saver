import React from "react";

const NotFound = () => (
  <div className="jumbotron jumbotron-fluid">
    <div className="container">
      <h1 className="display-4">Page Not Found</h1>
      <p className="lead">This page is unavailable</p>
    </div>
  </div>
);

export default NotFound;
